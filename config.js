var config = {
    database: 'SERVER_DATABASE',
    username: 'SERVER_USERNAME',
    password: 'SERVER_PASSWORD!',
    host: 'SERVER_HOST',
    port: SERVER_PORT
};

module.exports = config;