const model = require('../model');


let
    User = model.user,
    Work = model.work;

var CR_todoList = async (ctx, next) => {
    let username = ctx.session.username;
    if(username && Date.now() - ctx.session.time < 1000 * 60 * 30){
        var TodoListOpen = await Work.findAll({
            where : {
                username: username,
                status: "Open"
            }
        });
        var TodoListCompleted = await Work.findAll({
            where : {
                username: username,
                status: "Completed"
            }
        });
        if(TodoListOpen.length === 0 && TodoListCompleted.length === 0) {
            ctx.render('todolist.html', {
                noWork : true
            });
        } else {
            ctx.render('todolist.html', {
                workOpen : TodoListOpen,
                workCompleted : TodoListCompleted
            });
        }
    } else {
        ctx.response.redirect('/signin');
    }
};

module.exports = {
    'GET /todolist' : CR_todoList
};