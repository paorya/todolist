# Welcome Todolist
This is a sample project, the information is transmitted in clear text, the credentials are stored in plain text, and Anti-XSS is not available. Use in caution.
## Requirements

 - Node.js 11.0.0
 - Mysql 8 or higher
 - BootStrap 4 or higher

## QuickStart

 1. Install dependencies.
 `npm install`

 2. Make sure that bootstrap and its dependencies exist in the following directory.
	```
	/static/css/bootstrap.css
	/static/js/jQuery.js
	/ststic/js/Propper.js
	/static/js/bootstrap.js
	```
 3. Write the config of Mysql to the file.
`/config.js`
 4. Initialize the database. Note: Execute this line command, the original data table will be deleted.
`node initdb.js`
 5. Run the following code to start the server.
`node master.js`
 6. If everything is ok, the service will start at:
`http://localhost:8080/`
